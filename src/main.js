import Vue from 'vue'
import App from './App.vue'
import 'bootstrap/dist/css/bootstrap.min.css'
import router from './router'



new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
});

window.man = function() {
  console.log("El nombre de la familia que buscás es el mismo que figura en la página de Wikipedia");
}
