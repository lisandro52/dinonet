import Vue from 'vue'
import VueRouter from 'vue-router'
import 'bootstrap/dist/css/bootstrap.min.css'

import Home from './pages/home.vue'
import Login from './pages/login.vue'
import DinoNet from './pages/dinonet.vue'

Vue.use(VueRouter);

export default new VueRouter({
  routes: [
    { path: '/', component: Home },
    { path: '/login', component: Login },
    { path: '/net', component: DinoNet }
  ]
})

